import random
import copy
import math
import multiprocessing
from functools import partial
from Rule import Rule
from Node import Node

class AI:
    def __init__(self,rule,sim_num,node_layer,process_num):
        self.rule = rule
        self.sim_num = sim_num
        self.node_layer = node_layer
        self.process_num = process_num
        self.searching_tree= Node(self.rule.get_board_size())

    def predict_winner(self):
        sum_list = self.rule.get_board().count_board()
        winner = [sum([10^num for num in sum_list if num >0]),sum([10^(-num) for num in sum_list if num <0])]
        if winner[0]-winner[1] > 0:
            return 1
        elif winner[0]-winner[1] < 0:
            return -1
        else:
            return -11

    def make_decision(self,board,player,player_move,last_move):
        print "********************************"
        print "player " + str(player)
        self.rule.start_new_game(copy.deepcopy(board),player,player_move)
        return self.simulation(board,player,player_move,last_move)

    def get_move_by_score(self):
        move = self.searching_tree.get_best_move()
        print "move: " + str(move[0]) + ' by ' + str(move[1]) +' winning chance'
        return move[0]

    def get_move_by_times(self):
        move = self.searching_tree.get_most_move()
        print "move: " + str(move[0]) + ' by ' + str(move[1]) +' times'
        return move[0]

    def get_rule(self):
        return self.rule

    def simulate(self,loc,board,player,player_move):
        for idx in xrange(self.sim_num):
            self.rule.start_new_game(copy.deepcopy(board),player,player_move)
            cache_move_list= []
            cache_node_list = self.searching_tree
            if loc == None:
                loc = cache_node_list.get_random_node(self.rule.get_player(),self.rule.get_empty_loc_list())
            else:
                loc = loc
            cache_node_layer = 0
            self.rule.c_decision(loc)
            while self.rule.main() == 0 and cache_node_layer < self.node_layer:
                cache_move_list.append(loc)
                cache_node_list = cache_node_list.get_node(loc)
                loc = cache_node_list.get_random_node(self.rule.get_player(),self.rule.get_empty_loc_list())
                self.rule.c_decision(loc)
                cache_node_layer += 1
            winner = self.rule.main()
            win = [False,False]
            if (winner != -11) and (winner != 0):
                win[int((-0.5*winner)+0.5)] = True

            cache_node = self.searching_tree
            for idx in xrange(len(cache_move_list)):
                cache_node.update_win_chance(win,(len(cache_move_list)-idx))
                cache_node = cache_node.get_node(cache_move_list[idx])

    def simulation(self,board,player,player_move,last_move):
        if len(last_move) == 2:
            self.searching_tree = self.searching_tree.get_node(last_move)
            self.searching_tree.print_info("last_move")
        [self.searching_tree.add_node(loc) for loc in self.rule.get_empty_loc_list()]
        self.simulate(None,board,player,player_move)
        returning_move = self.get_move_by_times()
        self.searching_tree = self.searching_tree.get_node(returning_move)
        self.searching_tree.print_info('result')
        return returning_move
