import random
import sys
import copy

class Node:
    def __init__(self,size):
        self.move_weight = 0.1
        self.size = size
        self.contain = [[None for idx in xrange(self.size)] for jdx in xrange(self.size)]
        self.win_times = [0,0]
        self.play_times = [0,0]
        self.win_chance = [0.5,0.5]
        self.empty_nodes = []
        self.activate = False

    def update_win_chance(self,win,move_left):
        for idx in xrange(2):
            win_idx = -1.0
            self.play_times[idx] += 1
            if win[idx]:
                win_idx = 1.0
                self.win_times[idx] += 1

            self.win_chance[idx] += win_idx*self.move_weight/move_left

    def get_best_move(self):
        print self.print_info('best_move')
        highest_score = max([max([cache_node.get_win_chance()[idx] for x_row in self.contain for cache_node in x_row if cache_node != None]) for idx in xrange(len(self.win_chance))])
        returning_move_list = [[jdx,idx] for kdx in xrange(len(self.win_chance)) for idx in xrange(self.size) for jdx in xrange(self.size) if self.contain[idx][jdx] != None and self.contain[idx][jdx].get_win_chance()[kdx] == highest_score]
        returning_move = random.choice(returning_move_list)
        return [returning_move,highest_score]

    def get_most_move(self):
        print self.print_info('most_move')
        most_times = max([max([cache_node.get_win_times()[0][1] for cache_node in x_row if cache_node != None])for x_row in self.contain])
        returning_move_list = [[jdx,idx] for idx in xrange(self.size) for jdx in xrange(self.size) if self.contain[idx][jdx] != None and self.contain[idx][jdx].get_win_times()[0][1] == most_times]
        returning_move = random.choice(returning_move_list)
        return [returning_move,most_times]

    def print_info(self,msg):
        print "*******************************" + msg + "*******************************"
        print self.print_win_chance()
        print self.print_win_times()

    def get_win_times(self):
        return [[self.win_times[0],self.play_times[0]],[self.win_times[1],self.play_times[1]]]

    def print_win_times(self):
        msg = "------------win_times------------\n"
        for cache_contain in self.contain:
            for test_node in cache_contain:
                if test_node == None:
                    msg += "\tNone\t"
                else:
                    msg += str(test_node.get_win_times()) + "\t"
            msg += '\n'
        return msg

    def get_win_chance(self):
        return self.win_chance

    def print_win_chance(self):
        msg = "------------win_chance------------\n"
        for cache_contain in self.contain:
            for test_node in cache_contain:
                if test_node == None:
                    msg += "\tNone\t"
                else:
                    msg += "[" + str('%.3f'%test_node.get_win_chance()[0]) + ',' + str('%.3f'%test_node.get_win_chance()[1]) + "]\t"
            msg += '\n'
        return msg

    def get_contain(self):
        return self.contain

    def change_contain(self,contain):
        self.contain = contain

    def add_node(self,loc):
        if self.is_empty(loc):
            self.contain[loc[1]][loc[0]] = Node(self.size)
            #if loc in self.empty_nodes:
            try:
                self.empty_nodes.remove(loc)
            except:
                sys.exc_clear()

    def get_empty_nodes(self):
        return [[x,y] for x in xrange(self.size) for y in xrange(self.size) if self.is_empty([x,y])]

    def get_random_loc(self,player):
#        cache_random_list = [[x,y] for y in xrange(self.size) for x in xrange(self.size) if self.contain[y][x] != None \
#        for idx in xrange(int(self.contain[y][x].get_win_chance()[int((-0.5*player)+0.5)]*100))]
#        return random.choice(cache_random_list)
        #full_nodes = [[[x,y],self.contain[y][x].get_win_chance()[player_num]] for x in xrange(self.size) for y in xrange(self.size) if not self.is_empty([x,y])]
        #return full_nodes[self.get_random_idx([cahce_node[1] for cahce_node in full_nodes])][0]


        player_num = int((-0.5*player)+0.5)
        key = random.uniform(0,sum([node.get_win_chance()[player_num] for node_list in self.contain for node in node_list if node != None]))
        for y in xrange(self.size):
            for x in xrange(self.size):
                if not self.is_empty([x,y]):
                    key -= self.contain[y][x].get_win_chance()[player_num]
                    if key <= 0:
                        return [x,y]

        #for loc in [[x,y] for x in xrange(self.size) for y in xrange(self.size) if not self.is_empty([x,y])]:
        #    key -= self.contain[loc[1]][loc[0]].get_win_chance()[player_num]
        #    if key <= 0:
        #        return loc

    #    maximum = sum([node.get_win_chance()[player_num] for node_list in self.contain for node in node_list if node != None])

    #    if maximum > 0:
    #        key = random.uniform(0,maximum)
    #        cumulative_probability = 0.0
    #        #[[x,y]for y in xrange(self.size) for x in xrange(self.size) if self.contain[y][x] and ]
    #        for y in xrange(self.size):
    #            for x in xrange(self.size):
    #               if not self.is_empty([x,y]):
    #                    cumulative_probability += self.contain[y][x].get_win_chance()[player_num]
    #                    if cumulative_probability>=key:
    #                        return [x,y]

    def get_random_node(self,player,empty_loc_list):
        if not self.activate:
            self.empty_nodes = list(tuple(empty_loc_list))
            self.activate = True
        if len(self.empty_nodes) > 0:
            self.add_node(random.choice(self.empty_nodes))
        return max([self.get_random_loc(player),self.get_random_loc(-player)],key = lambda random_loc:self.get_node(random_loc).get_win_chance())


    def is_empty(self,loc):
        if self.contain[loc[1]][loc[0]] == None:
            return True
        else:
            return False

    def get_node(self,loc):
        self.add_node(loc)
        return self.contain[loc[1]][loc[0]]
